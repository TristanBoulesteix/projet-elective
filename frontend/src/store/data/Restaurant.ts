export default class Restaurant {
    readonly id
    readonly name
    readonly description


    constructor(id: string, name: string, description: string) {
        this.id = id
        this.name = name
        this.description = description
    }
}