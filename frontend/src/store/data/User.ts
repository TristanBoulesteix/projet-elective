export class User {
    jwt
    refreshToken
    readonly firstName
    readonly lastName
    readonly email
    readonly referalCode
    readonly referalCodeUse
    readonly role

    constructor(jwt: string, refreshToken: string, firstName: string, lastName: string, email: string, referalCode:string, referalCodeUse:string, role:string) {
        this.jwt = jwt
        this.refreshToken = refreshToken
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.referalCode = referalCode
        this.referalCodeUse =referalCodeUse
        this.role=role
    }
}