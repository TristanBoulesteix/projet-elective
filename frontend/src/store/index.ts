import Vue from 'vue'
import Vuex from 'vuex'
import {User} from "@/store/data/User";
import {LoginPopupState} from "@/store/data/LoginPopupState";
import Restaurant from "@/store/data/Restaurant";



Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        auth_enabled: LoginPopupState.CLOSED as LoginPopupState,
        user: JSON.parse(localStorage.getItem("user") || "null") as (User | null),
        restaurants: JSON.parse(localStorage.getItem("restaurants") || "null") as (Array<Restaurant>),
        idRestaurant: JSON.parse(localStorage.getItem("idRestaurant") || "null") as string,
        infosRestaurant : JSON.parse(localStorage.getItem("infosRestaurant") || "null") as (Restaurant | null),
        listCommand: JSON.parse(localStorage.getItem('listCommands') || "[]") as (Array<any>),
        idCommandRestaurant: JSON.parse(localStorage.getItem("idCommandRestaurant") || "null") as (string | null),
    },
    mutations: {
        setAuthEnabled(state) {
            state.auth_enabled = state.auth_enabled === LoginPopupState.CLOSED ? LoginPopupState.LOGIN : LoginPopupState.CLOSED
        },
        setListCommandAndRestaurant(state, listCommand){
            if(listCommand !== []){
                localStorage.setItem('listCommands', JSON.stringify(listCommand))
            }
            else {
                localStorage.removeItem('listCommands')
            }
        },
        setIdRestaurantCommand(state, id: string) {
            if(id != ""){
                state.idCommandRestaurant = id
                localStorage.setItem("idCommandRestaurant", JSON.stringify(id))
            }else{
                localStorage.removeItem("idCommandRestaurant")
            }
            state.idCommandRestaurant = id
        },
        register(state) {
            state.auth_enabled = LoginPopupState.REGISTER
        },
        login(state) {
            state.auth_enabled = LoginPopupState.LOGIN
        },
        setUser(state, user) {
            if (user !== null) {
                state.auth_enabled = LoginPopupState.CLOSED
                localStorage.setItem("user", JSON.stringify(user))
            } else {
                localStorage.removeItem("user")
            }
            state.user = user
        },

        setRestaurants(state, restaurants : Array<Restaurant>) {
            if (restaurants.length!=0) {
                state.restaurants = restaurants
                localStorage.setItem("restaurants", JSON.stringify(restaurants))
            } else {
                localStorage.removeItem("restaurants")
            }
            state.restaurants = restaurants
        },

        setIdRestaurant(state, id: string) {
            if(id != ""){
                state.idRestaurant = id
                localStorage.setItem("idRestaurant", JSON.stringify(id))
            }else{
                localStorage.removeItem("idRestaurant")
            }
            state.idRestaurant = id
        }
    },
    actions: {},
    modules: {},
    getters: {
        authEnabled: state => {
            return state.auth_enabled
        },
        user: state => {
            return state.user
        },
        restaurants: state=>{
            return state.restaurants
        },
        idRestaurant: state=>{
            return state.idRestaurant
        }
    }
})
