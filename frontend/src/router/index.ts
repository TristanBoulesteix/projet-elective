import Vue from 'vue'
import VueRouter, {RouteConfig} from 'vue-router'
import Home from '../views/Home.vue'
import Profile from '@/components/profile/Profile.vue'
import Restaurant from '@/components/restaurant/Restaurant.vue'
import RestaurantCard from '@/components/restaurant/RestaurantCard.vue'
import Livreur from '@/components/livreur/Livreur.vue'
import Forbidden from '@/components/Security/Forbidden.vue'
import Command from '@/components/command/Command.vue'
import Panier from '@/components/command/Panier.vue'
import RestaurantCommand from '@/components/restaurant/RestaurantCommand.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/user/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/restaurant',
    name: 'Restaurant',
    component: Restaurant
  },
  {
    path: '/restaurant/:restId',
    name: 'RestaurantCard',
    component: RestaurantCard
  },
  {
    path: '/livreur',
    name: 'Livreur',
    component: Livreur
  },
  {
    path: '/security',
    name: 'Security',
    component: Forbidden
  },
  {
    path: "/command",
    name: "Command",
    component: Command
  },
  {
    path: "/panier",
    name: "Panier",
    component: Panier
  },
  {
      path: '/restaurant/:restId/command',
      name: 'RestaurantCommand',
      component: RestaurantCommand
  }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
