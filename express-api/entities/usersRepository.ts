import sqlConnector from "../conf/sqlConnection";
import User from "./user";
import logsModel from "../conf/schema/logsSchema";
import {Schema} from "mongoose";

export default class UsersRepository {
    public static async createUser(user: User): Promise<object> {
        const query: string = "INSERT INTO [dbo].[users] ([firstname],[lastname],[email],[mdp],[role],[referral_code]) " +
            "VALUES ('" + user.firstname + "','" + user.lastname + "','" + user.email + "','" + user.mdp + "'," + user.role + ",'" + user.referalCode + "');"

        let successCreate: boolean = false;
        let message: string = ""

        await sqlConnector.getSQL().request().query(query).then(result => {

            message = "USER CREATE SUCCESSFULLY"
            successCreate = true
        }).catch(err => {
            console.error(err.message)
            message = err.message
            successCreate = false
        })

        return {
            successCreate: successCreate,
            message: message
        }
    }

    public static async loginUser(email: string, mdp: string): Promise<User | null> {
        const query: string = "SELECT [users].[id], [firstname], [lastname], [email], [role].[role], [referral_code], [referral_code_use] from [dbo].[users] INNER JOIN [dbo].[role] ON ([dbo].[users].[role] = [dbo].[role].[id]) WHERE [email]='" + email + "' AND [mdp]='" + mdp + "' AND [disabled] = 0;"

        let user: User | null = null
        let connected: boolean = false

        await sqlConnector.getSQL().request().query(query).then(result => {
            if (result.rowsAffected[0] != 0) {
                connected = true
                user = {
                    id: result.recordset[0]['id'] as number,
                    firstname: result.recordset[0]['firstname'] as string,
                    lastname: result.recordset[0]['lastname'] as string,
                    email: result.recordset[0]['email'] as string,
                    role_role: result.recordset[0]['role'] as string,
                    referalCode: result.recordset[0]['referral_code'] as string,
                    referalCodeUse: result.recordset[0]['referral_code_use'] as string
                }
                const log = new logsModel({
                    id_user: user.id,
                    login_date: Date.now()
                })
                log.save()
            }
        }).catch(err => {
            console.error(err.message)
        })

        return user
    }

    public static async saveRefreshToken(userId: number, refreshToken: string) {
        let expires = Math.floor((Date.now() / 1000) + +7 * 60 * 60 * 24)
        const query: string = "UPDATE [dbo].[users] SET [token] = '" + refreshToken + "', [expires] = '" + expires + "' WHERE [id] = '" + userId + "';";

        await sqlConnector.getSQL().request().query(query).then(result => {

        }).catch(err => {
            console.error(err.message)
        })
    }

    public static async selectOneUserByToken(refreshToken: string): Promise<User | null> {
        const query: string = "SELECT [users].[id], [firstname], [lastname], [email], [role].[role], [referral_code], [referral_code_use] from [dbo].[users] INNER JOIN [dbo].[role] ON ([dbo].[users].[role] = [dbo].[role].[id]) WHERE [token]='" + refreshToken + "';"

        let user: User | null = null

        await sqlConnector.getSQL().request().query(query).then(result => {
            if (result.rowsAffected[0] != 0) {
                user = {
                    id: result.recordset[0]['id'] as number,
                    firstname: result.recordset[0]['firstname'] as string,
                    lastname: result.recordset[0]['lastname'] as string,
                    email: result.recordset[0]['email'] as string,
                    role: result.recordset[0]['role'] as number,
                    referalCode: result.recordset[0]['referral_code'] as string,
                    referalCodeUse: result.recordset[0]['referral_code_use'] as string
                }
            }
        }).catch(err => {
            console.error(err.message)
        })

        return user
    }

    public static async selectRoleByRefreshToken(refreshToken: string): Promise<string> {
        const query: string = "SELECT [dbo].[role].[role] FROM [dbo].[users] INNER JOIN [dbo].[role] ON ([dbo].[users].[role] = [dbo].[role].[id]) WHERE [dbo].[users].[token] = '" + refreshToken + "';"

        let role: string = ""

        await sqlConnector.getSQL().request().query(query).then(result => {
            role = result.recordset[0]['role'] as string
        }).catch(err => {
            console.error(err.message)
        })

        return role
    }


    public static async deleteUserByRefreshToken(refreshToken: string): Promise<string> {
        const query: string = "DELETE FROM [dbo].[users] WHERE [token] = '" + refreshToken + "';"

        let deleteStatus: string = ""

        await sqlConnector.getSQL().request().query(query).then(result => {
            deleteStatus = 'true'
        }).catch(err => {
            console.error(err.message)
            deleteStatus = err.message
        })

        return deleteStatus
    }

    public static async deleteUserById(id: string): Promise<string> {
        const query: string = "DELETE FROM [dbo].[users] WHERE [id] = '" + id + "';"

        let deleteStatus: string = ""

        await sqlConnector.getSQL().request().query(query).then(_ => {
            deleteStatus = 'true'
        }).catch(err => {
            console.error(err.message)
            deleteStatus = err.message
        })

        return deleteStatus
    }

    public static async disableUserById(id: string): Promise<string> {
        const query: string = "UPDATE [dbo].[users] SET [disabled] = 1 WHERE [id] = '" + id + "';"

        let disableStatus: string = ""

        await sqlConnector.getSQL().request().query(query).then(_ => {
            disableStatus = 'true'
        }).catch(err => {
            console.error(err.message)
            disableStatus = err.message
        })

        return disableStatus
    }

    public static async getIdByRefreshToken(refreshToken: string): Promise<number> {
        const query: string = "SELECT [id] FROM [dbo].[users] WHERE [token]='" + refreshToken + "';"

        let id: number = 0

        await sqlConnector.getSQL().request().query(query).then(result => {
            id = result.recordset[0]['id'] as number
        }).catch(err => {
            console.error(err.message)
        })

        return id
    }

    public static async modifyUserByRefreshToken(user: User, refreshToken: string): Promise<string> {
        let query: string = ""

        if (user.referalCodeUse === undefined) {
            query = "UPDATE [dbo].[users] SET [firstname] = '" + user.firstname + "', [lastname] = '" + user.lastname + "', [email] = '" + user.email + "', [referral_code_use] = NULL WHERE [token]='" + refreshToken + "';"

        } else {

            query = "UPDATE [dbo].[users] SET [firstname] = '" + user.firstname + "', [lastname] = '" + user.lastname + "', [email] = '" + user.email + "', [referral_code_use] = '" + user.referalCodeUse + "' WHERE [token]='" + refreshToken + "';"
        }

        let modifyStatus: string = ""

        await sqlConnector.getSQL().request().query(query).then(result => {
            modifyStatus = "true"
        }).catch(err => {
            modifyStatus = err.message
        })

        return modifyStatus
    }

    public static async modifyUserById(firstName: string, lastName: string, id: string): Promise<string> {
        const query = "UPDATE [dbo].[users] SET [firstname] = '" + firstName + "', [lastname] = '" + lastName + "' WHERE [id]='" + id + "';"

        let modifyStatus: string = ""

        await sqlConnector.getSQL().request().query(query).then(_ => {
            modifyStatus = "true"
        }).catch(err => {
            modifyStatus = err.message
        })

        return modifyStatus
    }

    public static async selectAllUsers(): Promise<any> {
        const query = "SELECT * FROM [dbo].[users]"

        let users: any

        await sqlConnector.getSQL().request().query(query).then(result => {
            users = result.recordsets
        }).catch(err => {
            users = err.message
        })

        return users
    }

    public static async verifyReferalCodeUse(user: User): Promise<boolean> {
        const query = "SELECT * FROM [dbo].[users] INNER JOIN [dbo].[role] ON ([dbo].[users].[role] = [dbo].[role].[id]) WHERE [referral_code] = '" + user.referalCodeUse + "' AND [role].[role] = '" + user.role_role + "';"

        let number: boolean = false

        await sqlConnector.getSQL().request().query(query).then(result => {
            if (result.rowsAffected[0] != 0) {
                number = true
            }
        }).catch(err => {
            number = false
        })

        return number
    }

    // noinspection DuplicatedCode
    public static async getUserById(id: number): Promise<User | null> {
        const query = "SELECT * FROM [dbo].[users] WHERE [id] = '" + id + "';"

        let user: User | null = null

        await sqlConnector.getSQL().request().query(query).then(result => {
            if (result.rowsAffected[0] != 0) {
                user = {
                    id: result.recordset[0]['id'] as number,
                    firstname: result.recordset[0]['firstname'] as string,
                    lastname: result.recordset[0]['lastname'] as string,
                    email: result.recordset[0]['email'] as string,
                    role: result.recordset[0]['role'] as number,
                    referalCode: result.recordset[0]['referral_code'] as string,
                    referalCodeUse: result.recordset[0]['referral_code_use'] as string
                }
            }
        }).catch(err => {
            console.error(err.message)
        })

        return user
    }
}
