export default interface User {
    id?:number;
    firstname: string;
    lastname: string;
    email: string;
    mdp?: string;
    expires?: string;
    token?: string;
    role?: number;
    referalCode?: string;
    referalCodeUse?: string;
    role_role?: string
}