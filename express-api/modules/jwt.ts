import jwt from 'jsonwebtoken';
import env from 'dotenv';
import crypto from 'crypto';
import users from '../entities/usersRepository';
import User from "../entities/user";

env.config();

export default class JWT{
    public static createJWT(user: User){
        let token = jwt.sign({
            user: user
        }, process.env.JWT_SECRET as string, {expiresIn: '24h'})

        return token
    }

    public static verifyJWT(token: string){
        let check: string;

        try {
            check = jwt.verify(token, process.env.JWT_SECRET as string) as string
        } catch (err) {
            if(err.name == 'TokenExpiredError'){
                check = 'renew'
            } else {
                check = err.name + token
            }
        }
        return check
    }

    public static createRefreshToken(userId: number){
        const refreshToken = crypto.randomBytes(128).toString("base64");
        users.saveRefreshToken(userId, refreshToken);
        return refreshToken;
    }
}
