import menusModel from "../conf/schema/menusSchema";
import mongoose from "mongoose";

export default class Statistics{
    public static async getStats(commandes: any): Promise<any>{
        let storageID: string[] = []
        let storageQuantity: number[] = []
        let result: any[] = []
        for(let commande of commandes){
            for(let menu of commande.id_m){
                let id: string = menu.id.toString()
                if(!storageID.includes(id)){
                    storageID.push(id)
                    storageQuantity.push(0)
                }
                let index: number = storageID.indexOf(id)
                storageQuantity[index] = storageQuantity[index] + parseInt(menu.quantity)
            }
        }
        let i: number = 0
        for(let id of storageID){
            let values = await menusModel.findOne({_id:mongoose.Types.ObjectId(storageID[i])})
            let temporyJSON = {
                "menu": values,
                "quantity_buy": storageQuantity[i]
            }
            result.push(temporyJSON)
            i = i + 1
        }
        return result
    }
}