import express, {Application, Request, Response} from "express";
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app: Application = express();
const port = 3000;

// import router
import homeRouter from './routes/home';
import authRouter from './routes/auth';
import restaurantRouter from "./routes/restaurant";
import commercialRouter from "./routes/commercial";
import userRouter from "./routes/user";
import livreurRouter from "./routes/livreur";

//BDD_Connect
import {bddConnect} from "./conf/mongoDBConnection";
import sqlConnector from "./conf/sqlConnection";

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'Express API for JSONPlaceholder',
        version: '1.0.0',
        description:
            'This is a REST API application made with Express. It retrieves data from JSONPlaceholder.',
        license: {
            name: 'Licensed Under MIT',
            url: 'https://spdx.org/licenses/MIT.html',
        },
        contact: {
            name: 'JSONPlaceholder',
            url: 'https://jsonplaceholder.typicode.com',
        },
    },
    servers: [
        {
            url: 'http://localhost:3000',
            description: 'Development server',
        },
    ],
};

const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: ['./routes/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/', homeRouter);
app.use('/auth', authRouter);
app.use('/restaurant', restaurantRouter)
app.use('/commercial', commercialRouter)
app.use('/command', userRouter);
app.use('/livreur', livreurRouter);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

try {
    app.listen(port, (): void => {
        console.log(`Connected successfully on port ${port}`);
        sqlConnector.sqlConnector();
        bddConnect();
    });
} catch (error) {
    console.error(`Error occured: ${error.message}`);
}