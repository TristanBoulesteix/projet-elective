import {Router} from 'express';
import commandesModel from "../conf/schema/commandesSchema";
import mongoose from "mongoose";

const routes = Router();


/**
 * @swagger
 * /livreur/command:
 *   get:
 *     summary: Get all commands
 *     description: A livreur can see all commands in order to do action on them
 *     responses:
 *       200:
 *         description: A list of commands
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 commande:
 *                   type: boolean
 *                   example: true
 *                 commandes:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: string
 *                         description: The id of the command
 *                         example: 60daf3a38125ff044a105870
 *                       id_user:
 *                         type: integer
 *                         description: The id of the user
 *                         example: 1
 *                       id_restaurant:
 *                         type: string
 *                         description: The id of the command
 *                         example: 60daf3a38125ff044a105870
 *                       payed:
 *                         type: boolean
 *                         description: If the client has payed
 *                         example: true
 *                       accepted_restaurant:
 *                         type: boolean
 *                         description: If the restaurant has accpeted the command
 *                         example: true
 *                       accepted_livreur:
 *                         type: boolean
 *                         description: If the livreur has accpeted the command
 *                         example: true
 *                       date:
 *                         type: date
 *                         description: The date of the creation of the command
 *                         example: 2021-06-29T10:19:15.064Z
 *                       id_m:
 *                         type: array
 *                         items:
 *                           type: object
 *                           properties:
 *                             _id:
 *                               type: string
 *                               description: The id of the index in the array
 *                               example: 60daf3a38125ff044a105870
 *                             id:
 *                               type: string
 *                               description: The id of the menu
 *                               example: 60daf3a38125ff044a105870
 *                             nom:
 *                               type: string
 *                               description: The nom of the menu
 *                               example: Menu hamburger
 *                             quantity:
 *                               type: string
 *                               description: The quantity of the menu order by the client
 *                               example: 9
 */
routes.get('/command', async function(req, res, next){
    console.log('access')
    const commandes = await commandesModel.find()
    res.status(200).send({commande:true, commandes:commandes})
})

/**
 * @swagger
 * /livreur/command/{id}/accepted:
 *   get:
 *     summary: Accepted command by livreur
 *     description: A livreur can accepeted a command in order to deliver this command to the client
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the command.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: A list of commands
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 commande:
 *                   type: boolean
 *                   example: true
 */
routes.get('/command/:id/accepted', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    try {
        const commandes = await commandesModel.findOneAndUpdate({_id:id},{
            accepted_livreur: true
        })
        res.status(200).send({commande: true})
    } catch (e) {
        res.status(400).send({commande: false, error:e.message})
    }
})

/**
 * @swagger
 * /livreur/command/{id}/delivery:
 *   get:
 *     summary: The command is delivered
 *     description: A livreur can indicate that the command has been delivered and received by the client
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the command.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: A list of commands
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 commande:
 *                   type: boolean
 *                   example: true
 */
routes.get('/command/:id/delivery', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    try {
        const commandes = await commandesModel.findOneAndUpdate({_id:id},{
            delivery: true
        })
        res.status(200).send({commande: true})
    } catch (e) {
        res.status(400).send({commande: false, error:e.message})
    }
})

export default routes;