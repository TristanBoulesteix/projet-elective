import {Router} from 'express';
import restaurantsModel from "../conf/schema/restaurantsSchema";
import menusModel from "../conf/schema/menusSchema";
import mongoose from "mongoose";
import users from '../entities/usersRepository';
import utilities from '../modules/utilities'
import commandesModel from "../conf/schema/commandesSchema";

const routes = Router();

/**
 * @swagger
 * /command/restaurant:
 *   get:
 *     summary: Get all restaurants
 *     description: Show all restaurants and their data
 *     responses:
 *       200:
 *         description: A list of restaurants
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 restaurants:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: string
 *                         description: The id of the restaurant
 *                         example: 60daf3a38125ff044a105870
 *                       id_user:
 *                         type: string
 *                         description: A description of the restaurant
 *                         example: "A nice fast-food restaurant whose specialty is fruits salad."
 *                       id_restaurateur:
 *                         type: string
 *                         description: The ID of the restaurateur associated with the restaurant
 *                         example: 60daf3er8125ff04ff4a1058
 *
 */
routes.get('/restaurant', async function (req, res, next) {
    const restaurants = await restaurantsModel.find()
    res.status(200).send({restaurants: restaurants})
})

/**
 * @swagger
 * /command/restaurant/{id}:
 *   get:
 *     summary: A menu
 *     description: Get all menus associated with a restaurant
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the restaurant.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: A list of menus
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 menus:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: string
 *                         description: The id of the menu
 *                         example: 60daf3a38125ff044a105870
 *                       nom:
 *                         type: string
 *                         description: The name of the menu
 *                         example: "Salad with potatoes"
 *                       prix:
 *                         type: integer
 *                         description: The price of the menu
 *                         example: 22.30
 *                       description:
 *                          type: string
 *                          description: The description of the menu
 *                          example: "A delicious meal"
 *                       disponible:
 *                          type: boolean
 *                          description: Returns true if the menu is available
 *                       image:
 *                          type: string
 *                          description: The path of the illustration image for the restaurant
 *                       id_restaurant:
 *                          type: string
 *                          description: The ID of the restaurant associated to the menu
 *                       articles:
 *                          type: array
 *                          description: A list of article asociated with the menu
 *                          items:
 *                              type: object
 *                              properties:
 *                                  nom:
 *                                      type: string
 *                                      description: The name of the article
 *                                  prix:
 *                                      type: integer
 *                                      description: The price of the article
 *                                  description:
 *                                      type: string
 *                                      description: The description of the content of the article
 *                                  disponible:
 *                                      type: boolean
 *                                      description: Returns True if the article is available
 *                                  image:
 *                                      type: string
 *                                      description: The path of the illustration image for the restaurant
 *
 *
 *
 */
routes.get('/restaurant/:id', async function (req, res, next) {
    const id_restau = mongoose.Types.ObjectId(req.params.id)
    const menus = await menusModel.find({id_restaurant: id_restau})
    res.status(200).send({menus: menus})
})

/**
 * @swagger
 * /command/restaurant/{id}/validate:
 *   post:
 *     summary: Validate a command
 *     description: Create a command for a restaurant
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the restaurant.
 *         schema:
 *           type: string
 *       - in: body
 *         name: menus
 *         required: true
 *         schema:
 *           type: array
 *           description: An array of menus data
 *           items:
 *              type: object
 *              properties:
 *                  id:
 *                      type: string
 *                      description: The ID of the menu
 *                  nom:
 *                      type: string
 *                      description: The name of the menu
 *                  quantity:
 *                      type: integer
 *                      description: The number of menus commanded
 *
 *     responses:
 *       200:
 *         description: The new command inserted
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 validate:
 *                   type: boolean
 *                   description: True if the command was successfully
 *                 command:
 *                  type: object
 *                  description: The new command created
 *                  properties:
 *                      id_user:
 *                          type: integer
 *                          description: The id of the user who passed the command
 *                      id_restaurant:
 *                          type: string
 *                          description: The id of the restaurant associated to the command
 *                      id_m:
 *                          type: array
 *                          description: An array of menus data
 *                          items:
 *                              type: object
 *                              properties:
 *                                  id:
 *                                      type: string
 *                                      description: The ID of the menu
 *                                  nom:
 *                                      type: string
 *                                      description: The name of the menu
 *                                  quantity:
 *                                      type: integer
 *                                      description: The number of menus commanded
 *                      payed:
 *                          type: boolean
 *                          description: True if the command is payed
 *                      accepted_restaurant:
 *                          type: boolean
 *                          description: True if the command is accepted by the restaurant
 *                      accepted_livreur:
 *                          type: boolean
 *                          description: True if the command is accepted by a deliverer
 *                      delivery:
 *                          type: boolean
 *                          description: True if the command is delivered
 *                      date:
 *                          type: string
 *                          format: date
 *                          description: Date of the command
 *
 */
routes.post('/restaurant/:id/validate', async function (req, res, next) {
    const id_user = await users.getIdByRefreshToken(req.headers.refreshtoken as string)
    const id_restaurant = mongoose.Types.ObjectId(req.params.id)
    const id_m = req.body.menus
    const payed = false
    const accepted_restaurant = false
    const accepted_livreur = false
    const delivery = false
    const date = utilities.getTodayDate()
    const newCommandModel = new commandesModel({
        id_user,
        id_restaurant,
        id_m,
        payed,
        accepted_restaurant,
        accepted_livreur,
        delivery,
        date
    })
    await newCommandModel.save()
    res.status(200).send({validate: true, command: newCommandModel})
})

/**
 * @swagger
 * /command/commandes:
 *   get:
 *     summary: Get all commands
 *     description: Get a list of all commands
 *     responses:
 *       200:
 *         description: A list of commands
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 validate:
 *                   type: boolean
 *                   description: True if the command was successfully
 *                 commandes:
 *                  type: array
 *                  description: An array of commands
 *                  items:
 *                      type: object
 *                      properties:
 *                          id_user:
 *                              type: integer
 *                              description: The id of the user who passed the command
 *                          id_restaurant:
 *                              type: string
 *                              description: The id of the restaurant associated to the command
 *                          id_m:
 *                              type: array
 *                              description: An array of menus data
 *                              items:
 *                                  type: object
 *                                  properties:
 *                                      id:
 *                                          type: string
 *                                          description: The ID of the menu
 *                                      nom:
 *                                          type: string
 *                                          description: The name of the menu
 *                                      quantity:
 *                                          type: integer
 *                                          description: The number of menus commanded
 *                          payed:
 *                              type: boolean
 *                              description: True if the command is payed
 *                          accepted_restaurant:
 *                              type: boolean
 *                              description: True if the command is accepted by the restaurant
 *                          accepted_livreur:
 *                              type: boolean
 *                              description: True if the command is accepted by a deliverer
 *                          delivery:
 *                              type: boolean
 *                              description: True if the command is delivered
 *                          date:
 *                              type: string
 *                              format: date
 *                              description: Date of the command
 *
 */
routes.get('/commandes', async function (req, res, next) {
    const id_user = await users.getIdByRefreshToken(req.headers.refreshtoken as string)
    const commandes = await commandesModel.find({id_user: id_user})
    res.status(200).send({commandes: commandes})
})

/**
 * @swagger
 * /command/commande/{id}:
 *   get:
 *     summary: Get a command
 *     description: Get a command with its ID
 *     responses:
 *       200:
 *         description: The command corresponding to the ID
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 validate:
 *                   type: boolean
 *                   description: True if the command was successfully
 *                 command:
 *                  type: object
 *                  description: The new command created
 *                  properties:
 *                      id_user:
 *                          type: integer
 *                          description: The id of the user who passed the command
 *                      id_restaurant:
 *                          type: string
 *                          description: The id of the restaurant associated to the command
 *                      id_m:
 *                          type: array
 *                          description: An array of menus data
 *                          items:
 *                              type: object
 *                              properties:
 *                                  id:
 *                                      type: string
 *                                      description: The ID of the menu
 *                                  nom:
 *                                      type: string
 *                                      description: The name of the menu
 *                                  quantity:
 *                                      type: integer
 *                                      description: The number of menus commanded
 *                      payed:
 *                          type: boolean
 *                          description: True if the command is payed
 *                      accepted_restaurant:
 *                          type: boolean
 *                          description: True if the command is accepted by the restaurant
 *                      accepted_livreur:
 *                          type: boolean
 *                          description: True if the command is accepted by a deliverer
 *                      delivery:
 *                          type: boolean
 *                          description: True if the command is delivered
 *                      date:
 *                          type: string
 *                          format: date
 *                          description: Date of the command
 *
 */
routes.get('/commande/:id', async function (req, res, next) {
    const id_commande = mongoose.Types.ObjectId(req.params.id)
    const commande = await commandesModel.findOne({_id: id_commande})
    res.status(200).send({commande: commande})
})

/**
 * @swagger
 * /command/commande/{id}/edit:
 *   post:
 *     summary: Edit a command
 *     description: Edit a command a get the updated command
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the command.
 *         schema:
 *           type: string
 *       - in: body
 *         name: menus
 *         required: true
 *         schema:
 *           type: array
 *           description: An array of menus data
 *           items:
 *              type: object
 *              properties:
 *                  id:
 *                      type: string
 *                      description: The ID of the menu
 *                  nom:
 *                      type: string
 *                      description: The name of the menu
 *                  quantity:
 *                      type: integer
 *                      description: The number of menus commanded
 *     responses:
 *       200:
 *         description: The updated command
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 paid:
 *                   type: boolean
 *                   description: True if the command was successfully updated
 *                   exemple: true
 *                 result:
 *                  type: object
 *                  description: The updated command
 *                  properties:
 *                      id_user:
 *                          type: integer
 *                          description: The id of the user who passed the command
 *                      id_restaurant:
 *                          type: string
 *                          description: The id of the restaurant associated to the command
 *                      id_m:
 *                          type: array
 *                          description: An array of menus data
 *                          items:
 *                              type: object
 *                              properties:
 *                                  id:
 *                                      type: string
 *                                      description: The ID of the menu
 *                                  nom:
 *                                      type: string
 *                                      description: The name of the menu
 *                                  quantity:
 *                                      type: integer
 *                                      description: The number of menus commanded
 *                      payed:
 *                          type: boolean
 *                          description: True if the command is payed
 *                      accepted_restaurant:
 *                          type: boolean
 *                          description: True if the command is accepted by the restaurant
 *                      accepted_livreur:
 *                          type: boolean
 *                          description: True if the command is accepted by a deliverer
 *                      delivery:
 *                          type: boolean
 *                          description: True if the command is delivered
 *                      date:
 *                          type: string
 *                          format: date
 *                          description: Date of the command
 *       400:
 *          description: If an error has occured
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          paid:
 *                              type: boolean
 *                              description: Set to false. The update has failed
 *                              exemple: false
 *                          error:
 *                              type: string
 *                              description: The cause of the failure
 *
 */
routes.post('/commande/:id/edit', async function (req, res, next) {
    const id_user = await users.getIdByRefreshToken(req.headers.refreshtoken as string)
    const id_commande = mongoose.Types.ObjectId(req.params.id)
    const commande = await commandesModel.findOne({_id: id_commande})
    commande.id_m = req.body.menus
    try {
        const result = await commandesModel.findOneAndUpdate({_id: id_commande}, {
            id_m: commande.id_m
        })
        res.status(200).send({paid: true, result: result})
    } catch (e) {
        res.status(400).send({paid: false, error: e.message})
    }
})

/**
 * @swagger
 * /command/commande/{id}/delete:
 *   get:
 *     summary: Delete a command
 *     description: Delete a command with its ID and get the deleted command
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the command.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: The updated command
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 delete:
 *                   type: boolean
 *                   description: True if the command was successfully deleted
 *                   exemple: true
 *                 result:
 *                  type: object
 *                  description: The old deleted command
 *                  properties:
 *                      id_user:
 *                          type: integer
 *                          description: The id of the user who passed the command
 *                      id_restaurant:
 *                          type: string
 *                          description: The id of the restaurant associated to the command
 *                      id_m:
 *                          type: array
 *                          description: An array of menus data
 *                          items:
 *                              type: object
 *                              properties:
 *                                  id:
 *                                      type: string
 *                                      description: The ID of the menu
 *                                  nom:
 *                                      type: string
 *                                      description: The name of the menu
 *                                  quantity:
 *                                      type: integer
 *                                      description: The number of menus commanded
 *                      payed:
 *                          type: boolean
 *                          description: True if the command is payed
 *                      accepted_restaurant:
 *                          type: boolean
 *                          description: True if the command is accepted by the restaurant
 *                      accepted_livreur:
 *                          type: boolean
 *                          description: True if the command is accepted by a deliverer
 *                      delivery:
 *                          type: boolean
 *                          description: True if the command is delivered
 *                      date:
 *                          type: string
 *                          format: date
 *                          description: Date of the command
 *       400:
 *          description: If an error has occured
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          delete:
 *                              type: boolean
 *                              description: Set to false. The update has failed
 *                              exemple: false
 *                          error:
 *                              type: string
 *                              description: The cause of the failure
 *
 */
routes.get('/commande/:id/delete', async function (req, res, next) {
    const id_commande = mongoose.Types.ObjectId(req.params.id)
    try {
        const result = await commandesModel.findOneAndDelete({_id: id_commande})
        res.status(200).send({delete: true, result: result})
    } catch (e) {
        res.status(400).send({delete: false, error: e.message})
    }
})

/**
 * @swagger
 * /command/commande/{id}/pay:
 *   get:
 *     summary: Pay a command
 *     description: Set a command as payed
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the command.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: The updated command
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 paid:
 *                   type: boolean
 *                   description: True if the command was successfully deleted
 *                   exemple: true
 *                 result:
 *                  type: object
 *                  description: The old deleted command
 *                  properties:
 *                      id_user:
 *                          type: integer
 *                          description: The id of the user who passed the command
 *                      id_restaurant:
 *                          type: string
 *                          description: The id of the restaurant associated to the command
 *                      id_m:
 *                          type: array
 *                          description: An array of menus data
 *                          items:
 *                              type: object
 *                              properties:
 *                                  id:
 *                                      type: string
 *                                      description: The ID of the menu
 *                                  nom:
 *                                      type: string
 *                                      description: The name of the menu
 *                                  quantity:
 *                                      type: integer
 *                                      description: The number of menus commanded
 *                      payed:
 *                          type: boolean
 *                          description: True if the command is payed
 *                      accepted_restaurant:
 *                          type: boolean
 *                          description: True if the command is accepted by the restaurant
 *                      accepted_livreur:
 *                          type: boolean
 *                          description: True if the command is accepted by a deliverer
 *                      delivery:
 *                          type: boolean
 *                          description: True if the command is delivered
 *                      date:
 *                          type: string
 *                          format: date
 *                          description: Date of the command
 *       400:
 *          description: If an error has occured
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          paid:
 *                              type: boolean
 *                              description: Set to false because of failure
 *                              exemple: false
 *                          error:
 *                              type: string
 *                              description: The cause of the failure
 *
 */
routes.get('/commande/:id/pay', async function (req, res, next) {
    const id_commande = mongoose.Types.ObjectId(req.params.id)
    try {
        const result = await commandesModel.findOneAndUpdate({_id: id_commande}, {
            payed: true
        })
        res.status(200).send({paid: true, result: result})
    } catch (e) {
        res.status(400).send({paid: false, error: e.message})
    }
})

export default routes;