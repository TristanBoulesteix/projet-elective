import {Router} from 'express';
import jwt from '../modules/jwt';
import User from "../entities/user";
import users from '../entities/usersRepository';
import crypto from "crypto";
import logsModel from "../conf/schema/logsSchema";

const routes = Router();

/**
 * @swagger
 * /auth/verify:
 *   get:
 *     summary: Verify the JWT token
 *     description: Verify the JWT token and refresh the session if needed
 *     parameters:
 *       - in: header
 *         name: token
 *         required: true
 *         description: The JWT token of the user
 *         schema:
 *           type: string
*       - in: header
 *         name: refreshtoken
 *         required: true
 *         description: The token to refresh the JWT token
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: The response with the new tokens if needed
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 check:
 *                   type: boolean
 *                   description: True if the request was successful
 *                   exemple: true
 *                 token:
 *                  type: string
 *                  description: The new token (optional)
 *                 refreshtoken:
 *                  type: string
 *                  description: The new refreshtoken (optional)
 *
 */
routes.get('/verify', async function (req, res, next) {
    let check = jwt.verifyJWT(req.headers.token as string);
    if (check == 'renew') {
        let user = await users.selectOneUserByToken(req.headers.refreshtoken as string)
        if (user !== null) {
            let token = jwt.createJWT(user);
            let refreshToken = jwt.createRefreshToken(user.id as number);
            res.status(200).send({check: check, token: token, refreshToken: refreshToken});
        }
    } else {
        res.status(200).send({check: check});
    }
});

/**
 * @swagger
 * /auth/login:
 *   post:
 *     summary: Login
 *     description: Log a user into the api
 *     parameters:
 *       - in: body
 *         name: email
 *         required: true
 *         description: The email of the user
 *         schema:
 *           type: string
 *       - in: body
 *         name: mdp
 *         required: true
 *         description: The password of the user
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: The user data
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 connected:
 *                   type: boolean
 *                   description: True if the user was connected successfully
 *                   exemple: true
 *                 token:
 *                  type: string
 *                  description: The JWT token
 *                 refreshtoken:
 *                  type: string
 *                  description: The refreshtoken
 *                 user:
 *                  type: object
 *                  description: The user
 *                  properties:
 *                      firstname:
 *                          type: string
 *                      lastname:
 *                          type: string
 *                      email:
 *                          type: string
 *                      role:
 *                          type: string
 *                      referal_code:
 *                          type: string
 *                      referal_code_use:
 *                          type: string
 *       401:
 *         description: Error - unable to authentificate the user
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 connected:
 *                   type: boolean
 *                   description: True if the user was connected successfully
 *                   exemple: true
 *
 */
routes.post('/login', async function (req, res, next) {
    let email = req.body.email
    let mdp = req.body.mdp
    let user = await users.loginUser(email, mdp);
    let connected = user !== null
    if (user !== null) {
        let token = jwt.createJWT(user);
        let refreshToken = jwt.createRefreshToken(user.id as number);
        res.status(200).json({
            'connected': connected,
            token: token,
            refreshToken: refreshToken,
            user: {
                firstName: user.firstname,
                lastName: user.lastname,
                email: user.email,
                role: user.role_role,
                referal_code: user.referalCode,
                referal_code_use: user.referalCodeUse
            }
        })
    } else {
        res.status(401).send({'connected': connected})
    }
});

/**
 * @swagger
 * /auth/signup:
 *   post:
 *     summary: Register / Sign-up
 *     description: Register into the API / Create an account
 *     parameters:
 *       - in: body
 *         name: email
 *         required: true
 *         description: The email of the user
 *         schema:
 *           type: string
 *       - in: body
 *         name: mdp
 *         required: true
 *         description: The password of the user
 *         schema:
 *           type: string
 *       - in: body
 *         name: firstname
 *         required: true
 *         description: The first name of the user
 *         schema:
 *           type: string
 *       - in: body
 *         name: lastname
 *         required: true
 *         description: The last name of the user
 *         schema:
 *           type: string
 *       - in: body
 *         name: role
 *         required: true
 *         description: The ID of the role of the user
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: The user data
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 create:
 *                  type: object
 *                  description: The new user
 *                  properties:
 *                      firstname:
 *                          type: string
 *                      lastname:
 *                          type: string
 *                      email:
 *                          type: string
 *                      role:
 *                          type: string
 *                      referal_code:
 *                          type: string
 *                      referal_code_use:
 *                          type: string
 *
 */
routes.post('/signup', async function (req, res, next) {
    let params = req.body;
    let user: User = {
        firstname: params.firstname,
        lastname: params.lastname,
        email: params.email,
        mdp: params.mdp,
        role: params.role,
        referalCode: crypto.randomBytes(64).toString("base64")
    }
    let create = await users.createUser(user);
    res.status(200).send({create: create})
});

routes.get('/role', async function (req, res, next) {
    let role = await users.selectRoleByRefreshToken(req.headers.refreshtoken as string);
    res.status(200).send({role: role})
});

routes.get('/delete', async function (req, res, next) {
    let del = await users.deleteUserByRefreshToken(req.headers.refreshtoken as string);
    res.status(200).send({del: del})
})

routes.post('/modify', async function(req, res, next){
    let params = req.body.data;
    let user: User = {
        firstname: params.firstname as string,
        lastname: params.lastname as string,
        email: params.email as string,
        role_role: params.role as string,
        referalCodeUse: params.referal_code_use as string
    }
    if (user.referalCodeUse === ""){
        user.referalCodeUse = undefined
    }
    if(user.referalCodeUse){
        let verify = await users.verifyReferalCodeUse(user)
        if(verify){
            let modify = await users.modifyUserByRefreshToken(user, req.headers.refreshtoken as string);
            let response = await users.selectOneUserByToken(req.headers.refreshtoken as string)
            res.status(200).send({modify: modify, user:response})
        } else {
            res.status(200).send({modify: verify, errorrefcode:"Le code de parainnage est incorrecte ou apartient à un autre role que le votre"})
        }
    } else {
        let modify = await users.modifyUserByRefreshToken(user, req.headers.refreshtoken as string);
        let response = await users.selectOneUserByToken(req.headers.refreshtoken as string)
        res.status(200).send({modify: modify, user:response})
    }
})

routes.get('/users', async function (req, res, next) {
    const response = await users.selectAllUsers();
    res.status(200).send({response: response})
})

routes.get('/users/:id/delete/', async function (req, res, next) {
    const response = await users.deleteUserById(req.params.id)
    res.status(200).send({del: response})
})

routes.get('/users/:id/disable/', async function (req, res, next) {
    const response = await users.disableUserById(req.params.id)
    res.status(200).send({disable: response})
})

routes.post('/users/:id/modify/', async function (req, res, next) {
    const params = req.body;
    const modify = await users.modifyUserById(params.firstname, params.lastname, req.params.id);
    res.status(200).send({modify: modify})
})

routes.get('/logs', async function (req, res, next) {
    const logs = await logsModel.find()

    const data = []

    for (let logEntry of logs) {
        const user = await users.getUserById(logEntry.id_user)

        if (user !== null)
            data.push({
                first_name: user.firstname,
                last_name: user.lastname,
                login_date: logEntry.login_date
            })
    }

    res.status(200).send(data)
})

export default routes;