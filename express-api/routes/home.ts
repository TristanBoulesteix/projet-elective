import {Router} from 'express';

const routes = Router();

routes.get('/', function (req, res, next) {
    return res.status(200).send("message : Hi !" + req.socket.localAddress)
});

export default routes;