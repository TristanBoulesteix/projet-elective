import {Router} from "express";
import commandesModel from "../conf/schema/commandesSchema";
import Statistics from "../modules/statistics";
import menusModel from "../conf/schema/menusSchema";

const routes = Router();

routes.get('/statistics', async function (req, res, next) {
    try {
        const commands = await commandesModel.find()

        res.status(200).send({stats: true, data: await Statistics.getStats(commands)})
    } catch (e) {
        res.status(400).send({stats: false, error: e.message})
    }
})

routes.get('/commands/data', async function (req, res, next) {
    const commands: Array<any> = await commandesModel.find()

    const total: any[] = []

    for (let command of commands.filter(command => command.payed == true)) {
        for (let idMenu of command.id_m) {
            const menu: Array<any> = await menusModel.find({_id: idMenu.id})
            if (menu.length != 0)
                total.push(menu[0].prix * idMenu.quantity)
        }
    }

    const data = {
        commands_passed: commands.length,
        accepted_command: commands.reduce((number, currentValue) => number + (currentValue.accepted_restaurant == true), 0),
        deliverer_accepted_command: commands.reduce((number, currentValue) => number + (currentValue.accepted_livreur == true), 0),
        delivered: commands.reduce((number, currentValue) => number + (currentValue.delivery == true), 0),
        global_revenue: total.reduce((sum, currentValue) => sum + currentValue, 0) + " €"
    }

    res.status(200).send({data_stats: true, data: data})
})

export default routes