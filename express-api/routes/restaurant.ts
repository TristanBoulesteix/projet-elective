import {Router} from 'express';
import mongoose from 'mongoose';
import notifications from '../modules/notifications';
import users from '../entities/usersRepository';
import restaurantsModel from "../conf/schema/restaurantsSchema";
import menusModel from "../conf/schema/menusSchema";
import commandesModel from "../conf/schema/commandesSchema";
import stats from "../modules/statistics";

const routes = Router();

// For Restaurant


routes.get('/', async function(req, res, next){
    const id_restaurateur = await users.getIdByRefreshToken(req.headers.refreshtoken as string)
    try{
        const result = await restaurantsModel.find({id_restaurateur: id_restaurateur})
        res.status(200).send({restaurant:result})
    } catch (e) {
        res.status(400).send({restaurant:false})
    }
});

routes.get('/:id', async function(req, res, next){
    const id_restaurateur = await users.getIdByRefreshToken(req.headers.refreshtoken as string)
    const id_params = req.params.id as string
    try{
        const result = await restaurantsModel.find({id_restaurateur: id_restaurateur}).find({_id:mongoose.Types.ObjectId(id_params)})
        res.status(200).send({restaurant:result})
    } catch (e) {
        res.status(400).send({restaurant:false})
    }
});

routes.post('/', async function(req, res, next){
    console.log(req.body)
    const nom = req.body.nom as string
    const description = req.body.description as string
    const id_restaurateur = await users.getIdByRefreshToken(req.headers.refreshtoken as string)
    let newRestaurantModel = new restaurantsModel({nom, description, id_restaurateur})
    await newRestaurantModel.save()
    res.status(200).send({restaurant:newRestaurantModel})
});

routes.post('/edit/:id', async function(req, res, next){
    console.log('test')
    const nom = req.body.nom as string
    const description = req.body.description as string
    const id = mongoose.Types.ObjectId(req.params.id as string)
    try {
        const result = await restaurantsModel.findOneAndUpdate({_id: id}, {nom: nom, description: description})
        res.status(200).send({updated:true, result:result})
    } catch (e) {
        res.status(400).send({updated:false})
    }
});

routes.get('/delete/:id', async function(req, res, next){
    const id = req.params.id as string
    try{
        const result = await restaurantsModel.findOneAndDelete({_id: mongoose.Types.ObjectId(id)})
        res.status(200).send({delete:true, result:result})
    } catch (e) {
        res.status(400).send({delete:false})
    }
});

// For Menus

routes.get('/:id/menu', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    try{
        const result = await menusModel.find({id_restaurant: id})
        res.status(200).send({menus:result})
    } catch (e) {
        res.status(400).send({menus:false})
    }
})

routes.get('/:id/menu/:idm', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    try{
        const result = await menusModel.find({id_restaurant: id}).find({_id:id_menu})
        res.status(200).send({menu:result})
    } catch (e) {
        res.status(400).send({menu:false})
    }
})

routes.post('/:id/menu', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    const nom = req.body.nom as string
    const description = req.body.description as string
    const disponible = req.body.disponible as boolean
    const image = req.body.image as string
    const id_restaurant = id
    const articles = req.body.articles
    let prix: number = 0
    if(req.body.articles){
        for(let value of req.body.articles){
            prix = prix + parseInt(value.prix)
        }
    }
    const newMenuModel = new menusModel({nom, prix, description, disponible, image, id_restaurant, articles})
    await newMenuModel.save()
    res.status(200).send({menus:newMenuModel})
})

routes.post('/:id/menu/edit/:idm', async function(req, res, next){
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    console.log(req.body.disponible)
    try{
        let collections = await menusModel.findOne({_id:id_menu})
        if(req.body.nom){
            collections.nom = req.body.nom as string
        }
        if(req.body.description){
            collections.description = req.body.description as string
        }
        // if(req.body.disponible as boolean){
        //     console.log(req.body.disponible)
        //     collections.disponible = req.body.disponible as boolean
        // }
        if(req.body.image){
            collections.image = req.body.image as string
        }
        if(req.body.articles){
            collections.articles = req.body.articles
            collections.prix = 0
            for(let value of req.body.articles){
                collections.prix = collections.prix + parseInt(value.prix)
            }
        }
        const result = await menusModel.findOneAndUpdate({_id: id_menu}, {
            nom: collections.nom,
            description: collections.description,
            disponible: req.body.disponible,
            image: collections.image,
            articles: collections.articles,
            prix: collections.prix
        })
        res.status(200).send({updated:true, result:result})
    } catch (e) {
        res.status(400).send({updated:false, error:e.message})
    }
})

routes.get('/:id/menu/delete/:idm', async function(req, res, next){
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    try {
        const result = await menusModel.findOneAndDelete({_id:id_menu})
        res.status(200).send({delete:true, result:result})
    } catch (e) {
        res.status(400).send({delete:false, error:e.message})
    }
})

// For Articles

routes.get('/:id/menu/:idm/article/:ida', async function(req, res, next){
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    const id_article = mongoose.Types.ObjectId(req.params.ida as string)
    try {
        let collection = await menusModel.findOne({_id:id_menu})
        let result = {}
        if(collection.articles){
            let index = 0
            for(let value of collection.articles){
                if(id_article.equals(value._id)){
                    result = collection.articles[index]
                }
                index = index + 1
            }
        }
        res.status(200).send({article:true, result:result})
    } catch (e) {
        res.status(400).send({article:false,error:e.message})
    }
})

routes.post('/:id/menu/:idm/article', async function(req, res, next){
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    const nom = req.body.nom as string
    const prix = req.body.prix as number
    const description = req.body.description as string
    const disponible = req.body.disponible as boolean
    const image = req.body.image as string
    let article = {
        nom: nom,
        prix: prix,
        description: description,
        disponible: disponible,
        image: image
    }
    try {
        let collection = await menusModel.findOne({_id:id_menu})
        collection.articles.push(article)
        const result = await menusModel.findOneAndUpdate({_id: id_menu},{
            articles: collection.articles
        })
        res.status(200).send({created:true,articles: collection.articles})
    } catch (e) {
        res.status(400).send({created:false,error:e.message})
    }
})

routes.post('/:id/menu/:idm/article/edit/:ida', async function(req, res, next){
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    const id_article = mongoose.Types.ObjectId(req.params.ida as string)
    try {
        let result = {}
        let collection = await menusModel.findOne({_id:id_menu})
        if(collection.articles) {
            let index = 0
            for(let value of collection.articles){
                if(id_article.equals(value._id)){
                    let article = collection.articles[index]
                    let arts = collection.articles.filter((art: any) => !art._id.equals(id_article))
                    if(req.body.nom){
                        article.nom = req.body.nom
                    }
                    if(req.body.prix){
                        article.prix = req.body.prix
                    }
                    if(req.body.description){
                        article.description = req.body.description
                    }
                    if(req.body.disponible){
                        article.disponible = req.body.disponible
                    }
                    if(req.body.image){
                        article.image = req.body.image
                    }
                    arts.push(article)
                    let priceMenu = 0
                    for(let art of arts){
                        priceMenu = priceMenu + parseInt(art.prix)
                    }
                    result = await menusModel.findOneAndUpdate({_id: id_menu},{
                        prix: priceMenu,
                        articles: arts
                    })
                    res.status(200).send({modify:true,result:result})
                }
                index = index + 1
            }
        }
    } catch (e) {
        res.status(400).send({modify:false,error:e.message})
    }
})

routes.post('/:id/menu/:idm/article/delete/:ida', async function(req, res, next){
    const id_menu = mongoose.Types.ObjectId(req.params.idm as string)
    const id_article = mongoose.Types.ObjectId(req.params.ida as string)
    try {
        let result = {}
        let collection = await menusModel.findOne({_id:id_menu})
        if(collection.articles) {
            collection.articles = collection.articles.filter((art: any) => !art._id.equals(id_article))
            let priceMenu = 0
            for(let art of collection.articles){
                priceMenu = priceMenu + parseInt(art.prix)
            }
            result = await menusModel.findOneAndUpdate({_id: id_menu},{
                prix: priceMenu,
                articles: collection.articles
            })
        }
        res.status(200).send({deleted:true,result:result})
    } catch (e) {
        res.status(400).send({deleted:false,error:e.message})
    }
})

// For Commands

routes.get('/:id/command', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    const result = await commandesModel.find({id_restaurant:id})
    res.status(200).send({commands: result})
})

routes.post('/:id/command/:idc/validate', async function(req, res, next){
    const id_command = mongoose.Types.ObjectId(req.params.idc as string)
    try{
        const result = await commandesModel.findOneAndUpdate({_id:id_command}, {
            accepted_restaurant: true
        })
        res.status(200).send({validate:true,result:result})
    } catch (e) {
        res.status(400).send({validate:false,error:e.message})
    }
})

routes.get('/:id/command/:idc', async function(req, res, next){
    const id_command = mongoose.Types.ObjectId(req.params.idc as string)
    try{
        const result = await commandesModel.find({_id:id_command})
        res.status(200).send({selected:true,result:result})
    } catch (e) {
        res.status(200).send({selected:false,error:e.message})
    }
})

// For statistics

routes.get('/:id/statistics', async function(req, res, next){
    const id = mongoose.Types.ObjectId(req.params.id as string)
    try{
        const commandes = await commandesModel.find({id_restaurant:id})
        let statistics: any = await stats.getStats(commandes)
        res.status(200).send({stats:true,statistics:statistics})
    } catch (e) {
        res.status(400).send({stats:false,error:e.message})
    }
})

// For notifications

routes.get('/notification', async function(req, res, next){

})

export default routes;