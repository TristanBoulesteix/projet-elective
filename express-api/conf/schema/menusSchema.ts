import mongoose from 'mongoose';

const menusSchema = new mongoose.Schema({
    nom: String,
    prix: Number,
    description: String,
    disponible: Boolean,
    image: String,
    id_restaurant: mongoose.Types.ObjectId,
    articles: [{
        nom: String,
        prix: Number,
        description: String,
        disponible: Boolean,
        image: String,
    }]
})

const menusModel = mongoose.model('menu', menusSchema)

export default menusModel;