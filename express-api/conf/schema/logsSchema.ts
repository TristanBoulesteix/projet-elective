import mongoose from "mongoose";

const logsSchema = new mongoose.Schema({
    id_user: Number,
    login_date: Date
})

const logsModel = mongoose.model('logs', logsSchema)

export default logsModel