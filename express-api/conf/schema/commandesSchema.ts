import mongoose from 'mongoose';

const commandesSchema = new mongoose.Schema({
    id_user: Number,
    id_restaurant: mongoose.Types.ObjectId,
    id_m: [{
        id: mongoose.Types.ObjectId,
        nom: String,
        quantity: Number
    }],
    payed: Boolean,
    accepted_restaurant: Boolean,
    accepted_livreur: Boolean,
    delivery: Boolean,
    date: Date
})

const commandesModel = mongoose.model('commande', commandesSchema)

export default commandesModel;