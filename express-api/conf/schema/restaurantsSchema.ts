import mongoose from 'mongoose';

const restaurantsSchema = new mongoose.Schema({
    nom: String,
    description: String,
    id_restaurateur: Number
})

const restaurantsModel = mongoose.model('restaurant', restaurantsSchema)

export default restaurantsModel;