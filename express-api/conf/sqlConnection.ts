import {ConnectionPool} from 'mssql'
const dotenv= require('dotenv').config()

const sqlConfig = {
    user :  process.env.SQL_USER,
    password: process.env.SQL_PWD,
    database: process.env.SQL_DB_NAME,
    server: 'ceseat3.database.windows.net',
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    },
    options: {
        encrypt: true, // for azure
        trustServerCertificate: false // change to true for local dev / self-signed certs
    }
}

let sql : ConnectionPool;

export default class SQLConnector{
    public static sqlConnector(): void {
        sql = new ConnectionPool(sqlConfig)
        sql.connect(err => {
            if (err) {
                console.error(err.message);
            } else {
                console.log("SQL Connected")
            }
        })
    }

    public static getSQL(): ConnectionPool {
        return sql
    }
}