const mongoose = require('mongoose')
const dotenv= require('dotenv').config()

export function bddConnect(){
    const dbURL= `${process.env.MONGO_PORT}://${process.env.MONGO_USER}:${process.env.MONGO_PWD}@cesi.qz1or.mongodb.net/${process.env.MONGO_DB_NAME}`

    try{
        mongoose.connect(dbURL, {useNewUrlParser:true, useUnifiedTopology: true});
        console.log("MongoDB Connected")
    }catch(error){
        console.log(error);
    }
}
