const express = require('express');
const request = require('request');
const api = require('axios');
const cors = require('cors')

const servers = ['http://localhost:8081', 'http://localhost:8082' ];
let cur = 0;

let app = express();

const resolutionController = async (req, res, next) => {
    let data = {
        headers: req.headers
    }
    // VERIFY LIVREUR ACCESS
    if(!!req.path.match('(\/livreur\/command)$')){
        await api.get('http://localhost:8081/auth/verify', {
            headers: {
                token: req.headers.token,
                refreshtoken: req.headers.refreshtoken
            }
        }).then(async response => {
            console.log('response')
            if (response.data.check !== 'false') {
                await api.get('http://localhost:8082/auth/role', data).then(role => {
                    if (role.data.role === 'livreur') {
                        next()
                    } else {
                        return res.status(403).send({access: 'forbidden'})
                    }
                })
            } else {
                return res.status(401).send({tokenValid: 'false'})
            }
        }).catch(err => {
            console.log(err.message)
        })
    } else {
        next()
    }


}

const handler = (req, res) => {
    // Add an error handler for the proxied request
    const _req = request({ url: servers[cur] + req.url }).on('error', error => {
        res.status(500).send(error.message);
    });
    req.pipe(_req).pipe(res);
    cur = (cur + 1) % servers.length;
};
const server = app.use(cors()).use(resolutionController).get('*', handler).post('*', handler);

server.listen(8080);