﻿using System;
using System.Windows;
using Admin_CESEAT.Model;
using Admin_CESEAT.Model.Auth;

namespace Admin_CESEAT
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private async void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var loginResult = await ApiManager.Login(EmailField.Text, PasswordField.Password);

            switch (loginResult)
            {
                case User user:
                {
                    Hide();

                    if (user.Role == "commercial")
                    {
                        new Commercial.Commercial(user).Show();
                    }
                    else
                    {
                        new Tech.Tech(user, await ApiManager.GetLoginLogs()).Show();
                    }

                    break;
                }
                case string error:
                    ErrorLabel.Content = error;
                    break;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }
    }
}