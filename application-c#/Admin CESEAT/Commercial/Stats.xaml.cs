﻿using System.Collections.Generic;
using System.Windows;
using Menu = Admin_CESEAT.Model.Menu;

namespace Admin_CESEAT.Commercial
{
	/// <summary>
	/// Logique d'interaction pour Stats.xaml
	/// </summary>
	public partial class Stats
	{
		public Stats(IEnumerable<Menu> menus)
		{
			InitializeComponent();
			StatsListView.ItemsSource = menus;
		}
		
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (!(Window.GetWindow(this) is Commercial window)) return;
			
			window.TitleContent.Content = "Service commercial :";
			window.ContentPanel.Content = new OptionsCommercial();
		}
	}
}
