﻿using System;
using System.Windows;
using Admin_CESEAT.Model.Auth;

namespace Admin_CESEAT.Commercial
{
    /// <summary>
    /// Logique d'interaction pour Commercial.xaml
    /// </summary>
    public partial class Commercial
    {
        public Commercial(User user)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            Title = "Commercial - " + user.FirstName + " " + user.LastName;
        }
        
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }
    }
}