﻿using System;
using System.Windows;
using System.Windows.Threading;
using Admin_CESEAT.Model;

namespace Admin_CESEAT.Commercial
{
	/// <summary>
	/// Logique d'interaction pour CommandsRealTime.xaml
	/// </summary>
	public partial class CommandsRealTime
	{
		public CommandsRealTime()
		{
			InitializeComponent();

			var timer = new DispatcherTimer();
			timer.Tick += async (sender, e) =>
			{
				var data = await ApiManager.GetRealTimeStats();

				if (data == null) return;

				PassedCommands.Content = data.PassedCommands;
				AcceptedCommands.Content = data.AcceptedCommands;
				DeliveryAccepted.Content = data.AcceptedByDelivererCommands;
				Delivered.Content = data.Delivered;
				GlobalRevenue.Content = data.GlobalRevenue;
			};
			timer.Interval = new TimeSpan(0, 0, 10);
			timer.Start();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (!(Window.GetWindow(this) is Commercial window)) return;
			
			window.TitleContent.Content = "Service commercial :";
			window.ContentPanel.Content = new OptionsCommercial();
		}
	}
}