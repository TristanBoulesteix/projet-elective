﻿using System.Windows;
using Admin_CESEAT.Model;
using Admin_CESEAT.Model.Auth;

namespace Admin_CESEAT.Commercial
{
	/// <summary>
	/// Logique d'interaction pour EditUser.xaml
	/// </summary>
	public partial class EditUser
	{
		private User User { get; }

		public EditUser(User user)
		{
			User = user;
			WindowStartupLocation = WindowStartupLocation.CenterScreen;
			InitializeComponent();
			LastName.Text = user.LastName;
			FirstName.Text = user.FirstName;
		}

		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
		
		private async void ButtonConfirm_Click(object sender, RoutedEventArgs e)
		{
			if (!string.IsNullOrEmpty(FirstName.Text) || !string.IsNullOrEmpty(LastName.Text))
			{
				await ApiManager.UpdateUser(User, FirstName.Text, LastName.Text);
				Close();
			}
			else
			{
				MessageBox.Show("Les champs ne doivent pas être vides.");
			}
			
		}
	}
}
