﻿using System.Collections.Generic;
using System.Windows;
using Admin_CESEAT.Model;
using Admin_CESEAT.Model.Auth;
using Button = System.Windows.Controls.Button;

namespace Admin_CESEAT.Commercial
{
    /// <summary>
    /// Logique d'interaction pour UserAccount.xaml
    /// </summary>
    public partial class UserAccount
    {
        public UserAccount(IEnumerable<User> users)
        {
            InitializeComponent();
            UsersListView.ItemsSource = users;
        }

        private async void Delete_Click(object sender, RoutedEventArgs e)
        {
            await ApiManager.DeleteUser(GetUserFromSender(sender));
            UsersListView.ItemsSource = await ApiManager.GetAllClient();
            UsersListView.Items.Refresh();
        }

        private async void Update_Click(object sender, RoutedEventArgs e)
        {
            new EditUser(GetUserFromSender(sender)).ShowDialog();
            UsersListView.ItemsSource = await ApiManager.GetAllClient();
            UsersListView.Items.Refresh();
        }

        private static User GetUserFromSender(object sender)
        {
            var button = sender as Button;
            return button?.DataContext as User;
        }

        private async void Disable_Click(object sender, RoutedEventArgs e)
        {
            await ApiManager.DisableUser(GetUserFromSender(sender));
            UsersListView.ItemsSource = await ApiManager.GetAllClient();
            UsersListView.Items.Refresh();
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!(Window.GetWindow(this) is Commercial window)) return;
			
            window.TitleContent.Content = "Service commercial :";
            window.ContentPanel.Content = new OptionsCommercial();
        }
    }
}