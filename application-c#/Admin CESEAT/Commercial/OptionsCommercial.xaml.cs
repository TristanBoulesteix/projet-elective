﻿using System.Windows;
using Admin_CESEAT.Model;

namespace Admin_CESEAT.Commercial
{
    /// <summary>
    /// Logique d'interaction pour OptionsCommercial.xaml
    /// </summary>
    public partial class OptionsCommercial
    {
        private Commercial CommercialWindow => Window.GetWindow(this) as Commercial;

        public OptionsCommercial()
        {
            InitializeComponent();
        }

        private async void ButtonClients_Click(object sender, RoutedEventArgs e)
        {
            var allClient = await ApiManager.GetAllClient();
            
            CommercialWindow.TitleContent.Content = "Administration des utilisateurs";

            // ReSharper disable once PossibleNullReferenceException
            CommercialWindow.ContentPanel.Content = new UserAccount(allClient);
        }

        private async void ButtonStats_Click(object sender, RoutedEventArgs e)
        {
            var menus = await ApiManager.GetStats();
            
            CommercialWindow.TitleContent.Content = "Statistiques";
            CommercialWindow.ContentPanel.Content = new Stats(menus);
        }

        private void ButtonRealTime_Click(object sender, RoutedEventArgs e)
        {
            CommercialWindow.TitleContent.Content = "Tableau de bord en temps réel";
            CommercialWindow.ContentPanel.Content = new CommandsRealTime();
        }
    }
}