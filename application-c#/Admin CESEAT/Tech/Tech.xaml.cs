﻿using System;
using System.Collections.Generic;
using System.Windows;
using Admin_CESEAT.Model;
using Admin_CESEAT.Model.Auth;

namespace Admin_CESEAT.Tech
{
    /// <summary>
    /// Logique d'interaction pour Tech.xaml
    /// </summary>
    public partial class Tech
    {
        public Tech(User user, IEnumerable<Log> logs)
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            Title = "Technicien - " + user.FirstName + " " + user.LastName;
            LogsListView.ItemsSource = logs;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }
    }
}