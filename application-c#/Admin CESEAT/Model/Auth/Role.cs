﻿namespace Admin_CESEAT.Model.Auth
{
    public enum Role
    {
        Admin, User, Deliverer, Rest, Dev, Commercial, Tech
    }
}