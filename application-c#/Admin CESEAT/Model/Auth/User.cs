﻿using System;
using System.Linq;

namespace Admin_CESEAT.Model.Auth
{
    public class User
    {
        public string Id { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Email { get; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public string Disabled { get; }
        public string Role { get; }

        public bool IsValidForAuth => Role == "commercial" || Role == "technique";

        public User(string id, string firstName, string lastName, string email, string token, string refreshToken,
            string role, string disabled)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Token = token;
            RefreshToken = refreshToken;
            Disabled = disabled;
            Role = role;
        }
    }
}