﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Admin_CESEAT.Model.Auth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Admin_CESEAT.Model
{
    public static class ApiManager
    {
        public const string RootUrl = "http://localhost:8080/";

        public static readonly HttpClient Client = new HttpClient();

        private static User CurrentUser { get; set; }

        public static async Task<object> Login(string email, string password)
        {
            var response = await Client.PostAsJsonAsync(RootUrl + "auth/login", new
            {
                email,
                mdp = password
            });

            if (!response.IsSuccessStatusCode) return "Nom d'utilisateur ou mot de passe incorrect";

            var userResult = response.Content.ReadAsAsync<ResponseLogin>().Result;

            var user = userResult.User;
            user.Token = userResult.token;
            user.RefreshToken = userResult.RefreshToken;

            if (!user.IsValidForAuth)
                return "Le compte utilisé n'est ni un compte commercial ni un compte de technicien.";

            CurrentUser = user;

            Client.DefaultRequestHeaders.Add("token", CurrentUser.Token);
            Client.DefaultRequestHeaders.Add("refreshtoken", CurrentUser.RefreshToken);

            return user;
        }

        public static async Task<List<User>> GetAllClient()
        {
            var response = await Get("auth/users");

            if (!response.IsSuccessStatusCode) return new List<User>();

            var result = await response.Content.ReadAsStringAsync();

            // ReSharper disable once MethodHasAsyncOverload
            var json = (JObject) JsonConvert.DeserializeObject(result);

            return (json?["response"]?[0] as JArray)?.ToObject<List<User>>();
        }

        public static async Task<bool> DeleteUser(User user)
        {
            var response = await Get("auth/users/" + user.Id + "/delete/");
            return response.IsSuccessStatusCode;
        }

        public static async Task<bool> DisableUser(User user)
        {
            var response = await Get("auth/users/" + user.Id + "/disable/");
            return response.IsSuccessStatusCode;
        }

        public static async Task UpdateUser(User user, string firstName, string lastName)
        {
            await Post("auth/users/" + user.Id + "/modify", new
            {
                firstname = firstName,
                lastname = lastName
            });
        }

        public static async Task<List<Menu>> GetStats()
        {
            var response = await Get("commercial/statistics");

            if (!response.IsSuccessStatusCode) return new List<Menu>();

            var result = await response.Content.ReadAsStringAsync();

            // ReSharper disable once MethodHasAsyncOverload
            var json = (JObject) JsonConvert.DeserializeObject(result);

            return json?["stats"]?.ToObject<bool>() == true
                ? (json["data"] as JArray)?.ToObject<List<Menu>>()
                : new List<Menu>();
        }

        public static async Task<GlobalData> GetRealTimeStats()
        {
            var response = await Get("commercial/commands/data");

            if (!response.IsSuccessStatusCode) return null;

            var result = await response.Content.ReadAsStringAsync();

            // ReSharper disable once MethodHasAsyncOverload
            var json = (JObject) JsonConvert.DeserializeObject(result);

            return json?["data_stats"]?.ToObject<bool>() == true
                ? json["data"]?.ToObject<GlobalData>()
                : null;
        }

        public static async Task<List<Log>> GetLoginLogs()
        {
            var response = await Get("auth/logs");

            if (!response.IsSuccessStatusCode) return new List<Log>();

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<Log>>(result);
        }

        private static async Task<HttpResponseMessage> Get(string url)
        {
            return await Client.GetAsync(RootUrl + url);
        }

        private static async Task Post(string url, object data)
        {
            await Client.PostAsJsonAsync(RootUrl + url, data);
        }

        private class ResponseLogin
        {
            public readonly string RefreshToken;

            public readonly string token;

            public readonly User User;

            [JsonConstructor]
            private ResponseLogin(User user, string refreshtoken, string token)
            {
                User = user;
                RefreshToken = refreshtoken;
                this.token = token;
            }
        }
    }
}