using Newtonsoft.Json;

namespace Admin_CESEAT.Model
{
    public class Menu
    {
        [JsonProperty(PropertyName = "menu")] public MenuDataJson MenuData { get; set; }

        [JsonProperty(PropertyName = "quantity_buy")]
        public int Quantity { get; set; }

        public class MenuDataJson
        {
            [JsonProperty(PropertyName = "nom")] public string Name { get; set; }

            [JsonProperty(PropertyName = "description")]
            public string Description { get; set; }
        }
    }
}