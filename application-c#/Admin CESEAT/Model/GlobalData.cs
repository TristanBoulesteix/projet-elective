using Newtonsoft.Json;

namespace Admin_CESEAT.Model
{
    public class GlobalData
    {
        [JsonProperty(PropertyName = "commands_passed")]
        public string PassedCommands { get; set; }
        
        [JsonProperty(PropertyName = "accepted_command")]
        public string AcceptedCommands { get; set; }
        
        [JsonProperty(PropertyName = "deliverer_accepted_command")]
        public string AcceptedByDelivererCommands { get; set; }
        
        [JsonProperty(PropertyName = "delivered")]
        public string Delivered { get; set; }
        
        [JsonProperty(PropertyName = "global_revenue")]
        public string GlobalRevenue { get; set; }
    }
}