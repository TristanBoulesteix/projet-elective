using System;
using Newtonsoft.Json;

namespace Admin_CESEAT.Model
{
    public class Log
    {
        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }
        
        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }
        
        [JsonProperty(PropertyName = "login_date")]
        public DateTime Date { get; set; }
    }
}