# Projet élective

Pour lancer l'API :
	- docker-compose up --build --scale app=2

Pour lancer la platforme de load balancing:
	- npm run start

Pour lancer l'application VueJS:
	- npm run serve

Pour lancer l'application VueJS de DEV pour voir les composants réutilisable:
	- npm run serve

Pour l'application lourde:
	- le .exe est dans -> /application-c#/Release/Admin CESEAT.exe

Prérequis: 
	- Avoir Docker
	- Avoir NodeJS
	- Avoir TypeScript
	- Avoir vue
	- Exécuter la commande "npm install" sur chaque projet du repository

URL:
	- Pour acceder au site web: http://localhost:4000/
	- Pour acceder à la documentation de l'API : http://localhost:8080
